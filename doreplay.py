__author__ = 'junfeng'
import json
import re
import time
import os.path
from sys import stdin, stdout

from actionfun import mouse_move, mouse_wheel, \
    leftClick, leftDown, leftUp, rightClick, rightDown, rightUp, \
    key_down, key_tap, key_up

from handle_config import SETTINGS

from search_image import find_pic

ACTIONS_FILE = SETTINGS["settings"]["actionsfile"]
TIME_INTERVAL = float(SETTINGS["settings"]["timeinterval"]) * 1000 #miliseconds

RETRY_COUNT = SETTINGS["settings"]["retry_count"]
RETRY_SLEEP = SETTINGS["settings"]["retry_sleep"]
CHECK_KEY = SETTINGS["settings"]["check_key"]
IMAGE_PREFIX = SETTINGS["settings"]["image_prefix"]
IMAGE_PATH = SETTINGS["settings"]["image_path"]

in_encode = stdin.encoding
out_encode = stdout.encoding
NUMBER_RE = re.compile(r"^\d$")

def getaction():
    actions = None
    with open(ACTIONS_FILE, "rb") as f:
        actions = json.load(f)
    return actions



def doReplay(number):
    _ACTIONS = getaction()
    _LENGTH = len(_ACTIONS)
    i = 0
    j = 0
    k = 0
    length = _LENGTH
    actions = _ACTIONS
    while i < length:
        cur = actions[i]
        messageName = cur["MessageName"].split(" ")
        print cur["MessageName"]
        if "mouse" in messageName:
            #mouse actions
            pos = cur["Position"]
            x, y = tuple(map(int, pos[1:-1].split(", ")))
            if "left" in messageName:
                if "down" in messageName:
                    leftDown(x, y)
                else:
                    leftUp(x, y)
            elif "right" in messageName:
                if "down" in messageName:
                    rightDown(x, y)
                else:
                    rightUp(x, y)
            elif 'wheel' in messageName:
                down = True if cur['Wheel'] == -1 else False
                mouse_wheel(down=down)
            else:
                mouse_move(x, y)
        else:
            #keyborad actions
            if "down" in messageName:
                key = cur["Key"]
                ascii_value = cur["Ascii"]
                scanCode = cur["ScanCode"]
                keyId = cur["KeyID"]
                if NUMBER_RE.match(key):
                    key_tap(number[j])
                    j += 1
                    if j == len(number):
                        j = 0

                elif key == CHECK_KEY:
                    print "%dth check point..." % k
                    k += 1
                    filename = "{0}{1}.png".format(IMAGE_PREFIX, k)
                    for z in range(0, RETRY_COUNT):
                        if find_pic(filename):
                            break
                        time.sleep(RETRY_SLEEP)
                    if z == RETRY_COUNT:
                        return False

                else:
                    key_tap(keyId)
        if length - i > 1:
            next_ = actions[i+1]
            need_sleep = next_["Time"] - cur["Time"]
            if need_sleep > 500:
                if TIME_INTERVAL > need_sleep:
                    need_sleep = TIME_INTERVAL
                time.sleep(need_sleep/1000.0)
            #else:
                #time.sleep(.5)
        i += 1
    return True


if __name__ == "__main__":
    actions = getaction()
    for ac in actions:
        print ac
    doReplay("15109272398")
































