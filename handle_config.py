#!/usr/bin/env python2

import os
import os.path
import ConfigParser
from os import linesep
from sys import stdin, stdout

#os input output encode
in_encode = stdin.encoding
out_encode = stdout.encoding

CONFIG_FILE = "config.ini"

def get_config_info():
    config_parser = ConfigParser.SafeConfigParser()
    config_parser.read(CONFIG_FILE)
    settings = {}
    for s in config_parser.sections():
        settings[s] = {}
        for item in config_parser.items(s):
            settings[s][item[0]] = item[1]
    return settings

SETTINGS = get_config_info()

if __name__ == "__main__":
    settings = get_config_info()
    print in_encode, out_encode
    for s in settings:
        section = settings[s]
        for k in section:
            print k, section[k].decode(in_encode), type(section[k])




