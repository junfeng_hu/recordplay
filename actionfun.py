__author__ = 'junfeng'
# coding: utf-8
import time

import win32api
import win32con
import autopy
from autopy.mouse import LEFT_BUTTON, RIGHT_BUTTON

def mouse_wheel(down=True):
    if down:
        dwData = -120
    else:
        dwData = 120
    dwFlag = win32con.MOUSEEVENTF_WHEEL
    win32api.mouse_event(dwFlag, 0, 0, dwData, 0)
    time.sleep(.1)

def mouse_move(x, y):
    "mouse mouse cursor to position(x, y)"
    if x < 0 or y < 0:
        return
	#autopy.mouse.move(x, y)
    win32api.SetCursorPos((x, y))
    time.sleep(.1)

def leftClick(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.click(LEFT_BUTTON)
    time.sleep(.1)

def leftDown(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.toggle(True, LEFT_BUTTON)
    time.sleep(.1)

def leftUp(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.toggle(False, LEFT_BUTTON)
    time.sleep(.1)

def rightClick(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.click(RIGHT_BUTTON)
    time.sleep(.1)

def rightDown(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.toggle(True, RIGHT_BUTTON)
    time.sleep(.1)

def rightUp(x=-1, y=-1):
    mouse_move(x, y)
    autopy.mouse.toggle(False, RIGHT_BUTTON)
    time.sleep(.1)

def key_down(key):
    "hold down the key"
    autopy.key.toggle(key, down_or_up=True)
    time.sleep(.1)

def key_up(key):
    "release the key"
    autopy.key.toggle(key, down_or_up=False)
    time.sleep(.1)
def key_tap(key):
    autopy.key.tap(key)
    time.sleep(.1)