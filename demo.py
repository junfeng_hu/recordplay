#!/usr/bin/env python2

import os.path


import wx

import wx.lib.filebrowsebutton as filebrowse


provider = wx.SimpleHelpProvider()
wx.HelpProvider.Set(provider)









class SettingsDialog(wx.Dialog):
    def __init__(
            self, parent, ID, title, size=wx.DefaultSize, pos=wx.DefaultPosition,
            style=wx.DEFAULT_DIALOG_STYLE,
            ):



        pre = wx.PreDialog()#Dialog.py
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        self.PostCreate(pre)
        #wx.Dialog.__init__(self,parent,ID,title,pos,size,style)
        sizer = wx.BoxSizer(wx.VERTICAL)



        head = wx.StaticText(self, -1, "Settings Dialog")
        head.SetHelpText("set loginName, save folder")
        sizer.Add(head, 0, wx.ALIGN_CENTRE | wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)

        self.pathTcl = wx.TextCtrl(self, -1, "", size=(150,30))
        self.pathTcl.SetHelpText("Output File Save Folder")
        box.Add(self.pathTcl, 1, wx.ALIGN_CENTRE|wx.ALL, 25)

        chooseButton = wx.Button(self, -1, "Choose Path", size=(100, 30))
        chooseButton.SetHelpText("Choose Folder")
        box.Add(chooseButton, 0, wx.ALIGN_CENTRE|wx.ALL, 25)


        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)

        self.loginNameTcl = wx.TextCtrl(self, -1, "", size=(150, 30))
        self.loginNameTcl.SetHelpText("User Login to twitter")
        box.Add(self.loginNameTcl, 1, wx.ALIGN_CENTRE|wx.ALL, 25)

        label = wx.Button(self, -1, "LoginName", size=(100, 30),
                style=wx.BORDER_NONE)
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 25)

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP|wx.LEFT, 5)

        btnbox= wx.BoxSizer()

        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self, size=(70, 30))
            btnbox.Add(btn)

        saveButton = wx.Button(self, wx.ID_OK, "Save", size=(100, 30))
        saveButton.SetHelpText("Save settings to disk")
        saveButton.SetDefault()
        btnbox.Add(saveButton, 0, wx.LEFT, 100)

        cancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel", size=(100, 30))
        cancelButton.SetHelpText("Cancel,nothing to do")
        btnbox.Add(cancelButton, 0, wx.LEFT, 10)

        sizer.Add(btnbox, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        #event bind
        self.Bind(wx.EVT_BUTTON, self.OnChooseButton, chooseButton)
        self.Bind(wx.EVT_BUTTON, self.OnSaveButton, saveButton)

        self.SetSizer(sizer)
        #sizer.Fit(self)



    def OnChooseButton(self, e):
        dlg = wx.DirDialog(self, "Choose save directory:",
                        style=wx.DD_DEFAULT_STYLE
                        #| wx.DD_DIR_MUST_EXIST
                        #| wx.DD_CHANGE_DIR
                        )
        if dlg.ShowModal() == wx.ID_OK:
            self.pathTcl.SetValue(dlg.GetPath())
        dlg.Destroy()

    def OnSaveButton(self, e):
        path=self.pathTcl.GetValue()
        if path and os.path.isdir(path) and self.loginNameTcl.GetValue():
            e.Skip(True)

        else:
            megdlg = wx.MessageDialog(self, "Please choose a valid directory and enter loginName first.")
            megdlg.ShowModal()
            megdlg.Destroy()





class Frame(wx.Frame):

    def __init__(self, parent=None, title="twitscrape_gui"):
        wx.Frame.__init__(self, parent, -1, title, pos=wx.DefaultPosition, size=(500, 600), 
                style=wx.DEFAULT_FRAME_STYLE | wx.STAP_ON_TOP)
        self.savePath = None
        self.loginName = None
        self.scrapedUser = None
        self.scrapeOption = None
        self.afterSearch = False
        self.hasSettingsLoaded = False
        self.InitUI()
        self.Center()
        self.Show()

    def InitUI(self):
        self.SetMenu()
        sizer = wx.BoxSizer(wx.VERTICAL)

        box1 = wx.BoxSizer(wx.HORIZONTAL)

        self.searchTcl = wx.TextCtrl(self, -1, "", size=(300, 30))
        box1.Add(self.searchTcl, 1, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        searchButton = wx.Button(self, -1, "Search", size=(100, 30))
        box1.Add(searchButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        sizer.Add(box1, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 25)

        box2 = wx.BoxSizer(wx.HORIZONTAL)
        self.tweetsButton = wx.Button(self, -1, "0 tweets", size=(100, 30))
        box2.Add(self.tweetsButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        self.followersButton = wx.Button(self, -1, "0 followers", size=(100, 30))
        box2.Add(self.followersButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        self.followingsButton = wx.Button(self, -1, "0 followings", size=(100, 30))
        box2.Add(self.followingsButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        sizer.Add(box2, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.LEFT | wx.RIGHT, 25)

        box3 = wx.BoxSizer(wx.HORIZONTAL)#radio box
        allRadio = wx.RadioButton(self, -1, "all", wx.DefaultPosition,
                (100, 30), wx.RB_GROUP)
        tweetsRadio = wx.RadioButton(self, -1, "tweets", wx.DefaultPosition,
                (100, 30))
        followersRadio = wx.RadioButton(self, -1, "followers", wx.DefaultPosition,
                (100, 30))
        box3.Add(allRadio, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        box3.Add(tweetsRadio, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        box3.Add(followersRadio, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 25)
        sizer.Add(box3, 0, wx.GROW | wx.ALIGN_CENTER_VERTICAL | wx.ALL, 25)

        box4 = wx.BoxSizer(wx.HORIZONTAL)
        closeButton = wx.Button(self, -1, "Close", size=(100, 30))
        box4.Add(closeButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 50)
        scrapeButton = wx.Button(self, -1, "Scrape", size=(100, 30))
        box4.Add(scrapeButton, 0, wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, 50)
        sizer.Add(box4, 0, wx.GROW | wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 50)

        line = wx.StaticLine(self, -1, size=(450,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP|wx.LEFT, 50)

        box5 = wx.BoxSizer(wx.HORIZONTAL)
        self.logTcl = wx.TextCtrl(self, -1, "", size=(450, 180), style=wx.TE_MULTILINE |
                wx.TE_READONLY)
        #self.logTcl.SetValue("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        box5.Add(self.logTcl, 0, wx.ALIGN_CENTRE | wx.ALL, 25)
        sizer.Add(box5, 0, wx.GROW | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 25)


        #Bind
        self.Bind(wx.EVT_BUTTON, self.OnSearchButton, searchButton)

        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadioButton, allRadio)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadioButton, tweetsRadio)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadioButton, followersRadio)
        self.Bind(wx.EVT_BUTTON, self.OnCloseButton, closeButton)
        self.Bind(wx.EVT_BUTTON, self.OnScrapeButton, scrapeButton)



        self.SetSizer(sizer)









    def SetMenu(self):
        menuBar = wx.MenuBar()

        fileMenu = wx.Menu()
        exitItem = fileMenu.Append(wx.ID_EXIT, "Quit", "Quit application")
        menuBar.Append(fileMenu, "&File")

        editMenu = wx.Menu()
        settingsItem = editMenu.Append(wx.ID_ANY, "Settings", "Set some options")

        menuBar.Append(editMenu, "&Edit")

        aboutMenu = wx.Menu()
        aboutItem = aboutMenu.Append(wx.ID_ANY, "About", "About this")
        menuBar.Append(aboutMenu, "&About")


        self.SetMenuBar(menuBar)

        self.Bind(wx.EVT_MENU, self.OnQuit, exitItem)
        self.Bind(wx.EVT_MENU, self.LanchSettingsDialog,id=settingsItem.GetId())

    def OnQuit(self, e):
        self.Close()

    def LanchSettingsDialog(self, e):

        settingsdialog = SettingsDialog(self, -1, "settings", size=(400, 300))
        settingsdialog.CenterOnScreen()

        # this does not return until the dialog is closed.
        val = settingsdialog.ShowModal()
        # this does not return until the dialog is closed.
        if val == wx.ID_OK:
            self.savePath = settingsdialog.pathTcl.GetValue()
            self.loginName = settingsdialog.loginNameTcl.GetValue()

        settingsdialog.Destroy()


    def OnSearchButton(self, e):
        pass

    def OnRadioButton(self, e):
        print e.GetEventObject().GetLabel()

    def OnCloseButton(self, e):
        self.Close()

    def OnScrapeButton(self, e):
        pass














def main():
    app = wx.App()
    frame = Frame()
    app.MainLoop()

if __name__ == '__main__':
    main()
