# -*- coding: utf-8 -*-
#
#
from Queue import Queue
import functools
from threading import Thread
from sys import stdin, stdout
import json

import pythoncom
import pyHook
from pyHook.HookManager import HookConstants as _HookConstants

from handle_config import SETTINGS

_isMouseDown = False
mouseLeftDownId = _HookConstants.WM_LBUTTONDOWN
mouseLeftUpId = _HookConstants.WM_LBUTTONUP
mouseMoveId = _HookConstants.WM_MOUSEMOVE

#os input output encode
in_encode = stdin.encoding
out_encode = stdout.encoding
ACTIONS_FILE = SETTINGS["settings"]["actionsfile"]

def printMouseEvent(event):
    print "MessageName:", event.MessageName
    print "Message:", event.Message
    print "Time:", event.Time
    print "Window:", event.Window
    print "WindowName:", event.WindowName
    print "Position:", event.Position
    print "Wheel:", event.Wheel
    print "Injected:", event.Injected
    print "---"
def onMouseEvent(q, event):
    # 监听鼠标事件
    global _isMouseDown
    if event.Message == mouseMoveId and not _isMouseDown:
        return True
    if event.Message == mouseLeftDownId:
        _isMouseDown = True
    if event.Message == mouseLeftUpId:
        _isMouseDown =False
    q.put(event)
    return True
    # 返回 True 以便将事件传给其它处理程序
    # 注意，这儿如果返回 False ，则鼠标事件将被全部拦截
    # 也就是说你的鼠标看起来会僵在那儿，似乎失去响应了

def printKeyEvent(event):
    print "MessageName:", event.MessageName
    print "Message:", event.Message
    print "Time:", event.Time
    print "Window:", event.Window
    print "WindowName:", event.WindowName
    print "Ascii:", event.Ascii, chr(event.Ascii)
    print "Key:", event.Key
    print "KeyID:", event.KeyID
    print "ScanCode:", event.ScanCode
    print "Extended:", event.Extended
    print "Injected:", event.Injected
    print "Alt", event.Alt
    print "Transition", event.Transition
    print "---"

def onKeyboardEvent(q, event):
    # 监听键盘事件
    q.put(event)
    return True
def event2dict(event):

	return dict(
		MessageName=event.MessageName, Message=event.Message,
        Time=event.Time, Window=event.Window,
        WindowName=event.WindowName)

def save_to_file_consumer(q, windowIds=[]):
    print("start consumer threading...")
    exit = False
    with open(ACTIONS_FILE, "wb") as f:
        actions = []
        while True:
            event = q.get()
            if event == "Exit":
                q.task_done()
                break
            if  len(windowIds) > 0 and event.Window not in windowIds:
            	q.task_done()
            	continue
            #convert event to dict
            ed = event2dict(event)
            #write mouse event
            if event.MessageName.startswith("mouse"):
            	ed.update(
            		Injected=event.Injected, Position=str(event.Position),
            		Wheel=event.Wheel, type_="mouse")
                printMouseEvent(event)
            #write keyborad event
            else:
            	ed.update(
            		Ascii=event.Ascii, KeyID=event.KeyID, ScanCode=event.ScanCode,
            		Key=event.Key, Extended=event.Extended, Injected=event.Injected,
            		Alt=event.Alt, Transition=event.Transition, type_="keyboard")
                printKeyEvent(event)
            actions.append(ed)
            q.task_done()
        f.write(json.dumps(actions, encoding=in_encode))

    print("finish consumer threading...")

def main():
    q = Queue()
	# 创建一个“钩子”管理对象
    t = Thread(target=functools.partial(save_to_file_consumer, q))
    t.start()
    hm = pyHook.HookManager()

    # 监听所有键盘事件
    hm.KeyDown = functools.partial(onKeyboardEvent, q)
    # 设置键盘“钩子”
    hm.HookKeyboard()

    # 监听所有鼠标事件
    hm.MouseAll = functools.partial(onMouseEvent, q)
    # 设置鼠标“钩子”
    hm.HookMouse()

    # 进入循环，如不手动关闭，程序将一直处于监听状态
    try:
        pythoncom.PumpMessages()
    except Exception as e:
        q.put("Exit")
        q.join()
        raise e


if __name__ == "__main__":
    main()
