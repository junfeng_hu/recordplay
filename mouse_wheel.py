# coding: utf-8
import win32api
import win32con
def mouse_wheel(down=True):

    if down:
        dwData = -120
    else:
        dwData = 120
    dwFlag = win32con.MOUSEEVENTF_WHEEL
    win32api.mouse_event(dwFlag, 0, 0, dwData, 0)
    